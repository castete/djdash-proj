FROM node:16-alpine3.11
RUN npm install -g npm@8.0.0 
RUN npm install -g @angular/cli@12.2.9
WORKDIR /var/app
COPY package.json ./
RUN npm install
COPY . .

# Puerto entorno dev
# EXPOSE 4200
# Puerto livereload
# EXPOSE 49153
# Puerto Test
# EXPOSE 9876
# Puerto e2e
# EXPOSE 49152 
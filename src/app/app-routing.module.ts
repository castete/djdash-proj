import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { ClienteComponent } from './pages/cliente/cliente.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { EditsComponent } from './pages/edits/edits.component';
import { CantanteComponent } from './pages/mantenimiento/cantante/cantante.component';
import { GeneroComponent } from './pages/mantenimiento/genero/genero.component';
import { PagesComponent } from './pages/pages.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { ClientesComponent } from './pages/reportes/clientes/clientes.component';
import { VentasComponent } from './pages/reportes/ventas/ventas.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';

const routes: Routes = [
  {
    path: '', component:PagesComponent,
    children:[
      {path: 'inicio', component: DashboardComponent},
      {path: 'reportes/ventas', component: VentasComponent},
      {path: 'clientes', component: ClienteComponent},
      {path: 'reportes/clientes', component: ClientesComponent},
      {path: 'usuarios', component: UsuariosComponent},
      {path: 'perfil', component: PerfilComponent},
      {path: 'edits', component: EditsComponent},
      {path: 'interpretes', component: CantanteComponent},
      {path: 'generos', component: GeneroComponent},
      {path: '', redirectTo:'/inicio', pathMatch:'full'}
    ]
  },
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

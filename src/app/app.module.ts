import { SanctumInterceptorService } from './interceptors/sanctum-interceptor.service';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { LoginComponent } from './auth/login/login.component';
import { PagesComponent } from './pages/pages.component';
import { VentasComponent } from './pages/reportes/ventas/ventas.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { EditsComponent } from './pages/edits/edits.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { ClientesComponent } from './pages/reportes/clientes/clientes.component';
import { ClienteComponent } from './pages/cliente/cliente.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CantanteComponent } from './pages/mantenimiento/cantante/cantante.component';
import { GeneroComponent } from './pages/mantenimiento/genero/genero.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SidebarComponent,
    LoginComponent,
    PagesComponent,
    VentasComponent,
    UsuariosComponent,
    EditsComponent,
    NavbarComponent,
    PerfilComponent,
    ClientesComponent,
    ClienteComponent,
    GeneroComponent,
    CantanteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule, 
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgSelectModule,
  ],
  providers: [{
    provide:HTTP_INTERCEPTORS,
    useClass:SanctumInterceptorService,
    multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }

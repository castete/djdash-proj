import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  role:any;
  constructor() { }

  ngOnInit(): void {
    this.getRole();
  }

  getName(){
    return localStorage.getItem('name');
  }

  getRole(){
    let r:any = localStorage.getItem('role');
    this.role =  JSON.parse(r);
    // console.log(this.role);
  }

}

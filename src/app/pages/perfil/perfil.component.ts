import { UsuarioService } from './../../services/usuario.service';
import { Router } from '@angular/router';
import { Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
declare var $: any;

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {
  errors:any=[];
  usuarios:any=[];
  loadingModalUsuario:boolean=false;
  disbled=true;
  editmode= false;

  public usuarioForm = this._form.group({
    id:[''],
    email:['', [Validators.required]],
    password:['', [Validators.required]],
    name:['', [Validators.required]],
    role:[{value: '', disabled: true}, [Validators.required]],
  });

  constructor(
              private _router:Router,
              private _form:FormBuilder,
              private usuarioService: UsuarioService
  ) { }

  ngOnInit(): void {
    this.getInfo();
  }

  getInfo(){
    this.loadingModalUsuario = true;
    this.usuarioService.infoUser().subscribe(
      (data)=>{
        // console.log(data);
        let role:any = localStorage.getItem('role');
        let rol:any = JSON.parse(role);
        this.usuarioForm.patchValue({
          id :data.id,
          email :data.email,
          name :data.name,
          role :rol[0]
        });
        this.loadingModalUsuario = false;
      }, ({error})=>{
        this.loadingModalUsuario = false;
        this.errors = error.errors;
      }
    )
  }

  update(){
    this.errors =[];
    this.loadingModalUsuario = true;
    this.usuarioService.update(this.usuarioForm.value).subscribe(
      (data)=>{
        this.loadingModalUsuario = false;
        this.usuarioForm.reset();
        $('#modalUsuario').modal('hide');
        Swal.fire({
          title: 'Satisfactorio',
          text: `${data.message}`,
          icon: 'success',
          confirmButtonText: 'Aceptar',
        }).then((result) => {
            this.getInfo();
        })
      },
      ({error})=>{
        this.loadingModalUsuario = false;
        console.log(error.message);
        this.errors = error.errors
      }
    )
  }
}

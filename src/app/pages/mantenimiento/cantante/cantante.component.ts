import { FormBuilder, Validators } from '@angular/forms';
import { SingerService } from './../../../services/singer.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
declare var $: any;

@Component({
  selector: 'app-cantante',
  templateUrl: './cantante.component.html',
  styleUrls: ['./cantante.component.scss']
})
export class CantanteComponent implements OnInit {

  errors:any=[];
  cantantes:any=[];
  loadingModalCantante:boolean=false;
  editmode= false;

  public cantanteForm = this._form.group({
    id:[''],
    name:['', [Validators.required]],
  });

  constructor(private singerService : SingerService, private _form:FormBuilder) { }

  ngOnInit(): void {
    this.all();
  }

  all(){
    this.singerService.all().subscribe(
      (data)=>{
        console.log(data);
        this.cantantes = data;
      },
      ({error})=>{console.log(error.message);}
    )
  }

  openNewModal(){
    this.errors = [];
    this.editmode = false;
    this.cantanteForm.reset();
    $('#cantanteModal').modal('show');
  }

  crear(){
    this.errors =[];
    this.loadingModalCantante = true;
    this.singerService.create(this.cantanteForm.value).subscribe(
      (data)=>{
        this.loadingModalCantante=false;
        this.cantanteForm.reset();
        $('#cantanteModal').modal('hide');
        Swal.fire({
          title: 'Satisfactorio',
          text: `${data.message}`,
          icon: 'success',
          confirmButtonText: 'Aceptar',
        }) .then((result) => {
            this.all();
        })
      },
      ({error})=>{
        this.loadingModalCantante=false;
        this.errors = error.errors
        // console.log(this.errors);
      }
    )
  }

  openModalEdit(row:any){
    this.errors = [];
    this.editmode= true;
    console.log(row);
    $('#cantanteModal').modal('show');
    this.cantanteForm.setValue({
      id :row.id,
      name :row.name
    });
  }

  update(){
    this.errors =[];
    this.loadingModalCantante = true;
    this.singerService.update(this.cantanteForm.value).subscribe(
      (data)=>{
        this.loadingModalCantante = false;
        console.log(data);
        this.cantanteForm.reset();
        $('#cantanteModal').modal('hide');
        Swal.fire({
          title: 'Satisfactorio',
          text: `${data.message}`,
          icon: 'success',
          confirmButtonText: 'Aceptar',
        }).then((result) => {
            this.all();
        })
      },
      ({error})=>{
        this.loadingModalCantante = false;
        console.log(error.message);
        this.errors = error.errors
      }
    )
  }

}

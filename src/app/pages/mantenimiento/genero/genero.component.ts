import { FormBuilder, Validators } from '@angular/forms';
import { GenderService } from './../../../services/gender.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
declare var $: any;

@Component({
  selector: 'app-genero',
  templateUrl: './genero.component.html',
  styleUrls: ['./genero.component.scss']
})
export class GeneroComponent implements OnInit {

  errors:any=[];
  generos:any=[];
  loadingModalGenero:boolean=false;
  editmode= false;

  public generoForm = this._form.group({
    id:[''],
    name:['', [Validators.required]],
  });

  constructor(private genderService:GenderService, private _form:FormBuilder) { }

  ngOnInit(): void {
    this.all();
  }

  all(){
    this.genderService.all().subscribe(
      (data)=>{
        console.log(data);
        this.generos = data;
      },
      ({error})=>{console.log(error.message);}
    )
  }

  openNewModal(){
    this.errors = [];
    this.editmode = false;
    this.generoForm.reset();
    $('#generoModal').modal('show');
  }


  crear(){
    this.errors =[];
    this.loadingModalGenero = true;
    this.genderService.create(this.generoForm.value).subscribe(
      (data)=>{
        this.loadingModalGenero=false;
        this.generoForm.reset();
        $('#generoModal').modal('hide');
        Swal.fire({
          title: 'Satisfactorio',
          text: `${data.message}`,
          icon: 'success',
          confirmButtonText: 'Aceptar',
        }) .then((result) => {
            this.all();
        })
      },
      ({error})=>{
        this.loadingModalGenero=false;
        this.errors = error.errors
        // console.log(this.errors);
      }
    )
  }

  openModalEdit(row:any){
    this.errors = [];
    this.editmode= true;
    console.log(row);
    $('#generoModal').modal('show');
    this.generoForm.setValue({
      id :row.id,
      name :row.name
    });
  }

  update(){
    this.errors =[];
    this.loadingModalGenero = true;
    this.genderService.update(this.generoForm.value).subscribe(
      (data)=>{
        this.loadingModalGenero = false;
        console.log(data);
        this.generoForm.reset();
        $('#generoModal').modal('hide');
        Swal.fire({
          title: 'Satisfactorio',
          text: `${data.message}`,
          icon: 'success',
          confirmButtonText: 'Aceptar',
        }).then((result) => {
            this.all();
        })
      },
      ({error})=>{
        this.loadingModalGenero = false;
        console.log(error.message);
        this.errors = error.errors
      }
    )
  }

}

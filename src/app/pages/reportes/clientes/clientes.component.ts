import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {

  clientes:any=[];

  constructor(private clienteService : ClienteService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    this.clienteService.all().subscribe(
      (data)=>{
        console.log(data);
        this.clientes = data;
      },({error})=>{
        console.log(error);
      }
    )
  }

}

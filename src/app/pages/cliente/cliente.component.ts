import { ClienteService } from 'src/app/services/cliente.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit {
  clientes:any=[];

  constructor(private clienteService : ClienteService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    this.clienteService.all().subscribe(
      (data)=>{
        console.log(data);
        this.clientes = data;
      },({error})=>{
        console.log(error);
      }
    )
  }

}

import { GenderService } from './../../services/gender.service';
import { SingerService } from './../../services/singer.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { GlobalVariable } from 'src/app/variables/meses';
import { MusicService } from 'src/app/services/music.service';
declare var $: any;
import Swal from 'sweetalert2';


@Component({
  selector: 'app-edits',
  templateUrl: './edits.component.html',
  styleUrls: ['./edits.component.scss']
})
export class EditsComponent implements OnInit {

  mes:any;
  meses:any= [];
  editmode=false;
  loadingModalMusica =false;
  errors:any=[];
  nameFilePreview='No seleccionado';
  nameFileEdit='No seleccionado';

  generos:any=[];
  cantantes:any=[];

  selectFilePreview = false;
  selectFileEdit = false;

  edits:any= [];

  public musicaForm = this._form.group({
    id :[''],
    name :['',[Validators.required]],
    url :['',[Validators.required]],
    bpm :['',[Validators.required]],
    url_preview :['',[Validators.required]],
    price :['',[Validators.required]],
    id_gender :['',[Validators.required]],
    id_singer :['',[Validators.required]],
  });

  constructor(
    private _router:Router,
    private _form:FormBuilder,
    private singerService : SingerService,
    private genderService:GenderService,
    private musicService: MusicService
  ) {
    // console.log(GlobalVariable.meses);
    this.meses = GlobalVariable.meses;
  }

  ngOnInit(): void {
    this.all();
    this.getCantantes();
    this.getGeneros();
  }

  openModalNuevo(){
    this.errors = [];
    this.editmode = false;
    this.musicaForm.reset();
    $('#modalMusica').modal('show');
  }


  all(){
    this.musicService.all().subscribe(
      (data)=>{
        this.edits = data;
      },
      (error)=>{console.log(error.message);}
    )
  }
  crear(){
    this.errors=[];
    this.loadingModalMusica = true;
    let formData = new FormData();
    formData.append('name', this.musicaForm.get('name')?.value);
    formData.append('url', this.musicaForm.get('url')?.value);
    formData.append('bpm', this.musicaForm.get('bpm')?.value);
    formData.append('url_preview', this.musicaForm.get('url_preview')?.value);
    formData.append('price', this.musicaForm.get('price')?.value);
    formData.append('id_gender', this.musicaForm.get('id_gender')?.value);
    formData.append('id_singer', this.musicaForm.get('id_singer')?.value);

    
    this.musicService.create(formData).subscribe(
      (data)=>{
        this.loadingModalMusica = false;
        $('#modalMusica').modal('hide');
        Swal.fire({
          title: 'Satisfactorio',
          text: `${data.message}`,
          icon: 'success',
          confirmButtonText: 'Aceptar',
        }) .then((result) => {
            this.all();
        })
      },
      ({error})=>{
        this.loadingModalMusica = false;
        this.errors = error.errors

      }
    )
  };
  openModalEdit(row:any){
    this.errors = [];
    this.editmode= true;
    this.musicaForm.reset();
    $('#modalMusica').modal('show');
    this.musicaForm.patchValue({
      id :row.id,
      name :row.name,
      bpm :row.bpm,
      price :row.price,
      id_gender :row.id_gender,
      id_singer :row.id_singer,
    });
  }
  update(){
    this.errors =[];
    this.loadingModalMusica = true;
    console.log(this.musicaForm.value);
    let form = new FormData();
    form.append('id', this.musicaForm.get('id')?.value);
    form.append('name', this.musicaForm.get('name')?.value);
    form.append('url', this.musicaForm.get('url')?.value);
    form.append('bpm', this.musicaForm.get('bpm')?.value);
    form.append('url_preview', this.musicaForm.get('url_preview')?.value);
    form.append('price', this.musicaForm.get('price')?.value);
    form.append('id_gender', this.musicaForm.get('id_gender')?.value);
    form.append('id_singer', this.musicaForm.get('id_singer')?.value);

    console.log(form);
    this.musicService.update(form).subscribe(
      (data)=>{
        this.loadingModalMusica = false;
        console.log(data);
        this.musicaForm.reset();
        $('#modalMusica').modal('hide');
        Swal.fire({
          title: 'Satisfactorio',
          text: `${data.message}`,
          icon: 'success',
          confirmButtonText: 'Aceptar',
        }).then((result) => {
            this.all();
        })
      },
      ({error})=>{
        this.loadingModalMusica = false;
        console.log(error.message);
        this.errors = error.errors
      }
    )
  };


  selectPreview(event:any){
    if(event.target.files.length > 0) 
    {
      this.selectFilePreview =true;
      console.log(event.target.files[0].name);
      this.nameFilePreview =event.target.files[0].name;
      this.musicaForm.patchValue({
        url_preview :event.target.files[0]
      });

    }
  }
  selectEdit(event:any){
    if(event.target.files.length > 0) 
    {
      this.selectFileEdit = true;
      this.nameFileEdit = event.target.files[0].name;

      this.musicaForm.patchValue({
        url :event.target.files[0]
      });
    }
  }


  downloadFile(row:any){
    this.musicService.descarga(row).subscribe(response =>{
      console.log(response.data);
      let nameFile = `(${row.bpm})-${row.name}-${row.interprete}`
      this.descargar(response,nameFile);
    })
  }


  descargar(response:any, filename:string):void{
    const dataType =response.type;
    var fileExtension = dataType.split('/').pop();
    console.log(fileExtension); 
    const binaryData = [];
    binaryData.push(response);
    const filepath = window.URL.createObjectURL(new Blob(binaryData,{type:dataType}));
    const downloadLink = document.createElement('a');
    downloadLink.href = filepath;
    downloadLink.setAttribute('download', `${filename}.${fileExtension}`),
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

  getCantantes(){
    this.singerService.all().subscribe(
      (data)=>{
        this.cantantes = data;
      }, ({error})=>{
        console.log(error.message);
      }
    )
  }
  getGeneros(){
    this.genderService.all().subscribe(
      (data)=>{
        this.generos=data;
      }, ({error})=>{
        console.log(error.message);
      }
    )
  }

}

import { UsuarioService } from './../../services/usuario.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
declare var $: any;

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {

  errors:any=[];
  usuarios:any=[];
  loadingModalUsuario:boolean=false;
  editmode= false;

  public usuarioForm = this._form.group({
    id:[''],
    email:['', [Validators.required]],
    password:['', [Validators.required]],
    name:['', [Validators.required]],
  });

  constructor(
              private _router:Router,
              private _form:FormBuilder,
              private usuarioService: UsuarioService
  ) { }



  ngOnInit(): void {
    this.all();
  }

  openNewModal(){
    this.errors = [];
    this.editmode = false;
    this.usuarioForm.reset();
    $('#modalUsuario').modal('show');

  }

  crear(){
    this.errors =[];
    this.loadingModalUsuario = true;
    this.usuarioService.register(this.usuarioForm.value).subscribe(
      (data)=>{
        this.loadingModalUsuario=false;
        this.usuarioForm.reset();
        $('#modalUsuario').modal('hide');
        Swal.fire({
          title: 'Satisfactorio',
          text: `${data.message}`,
          icon: 'success',
          confirmButtonText: 'Aceptar',
        }) .then((result) => {
            this.all();
        })
      },
      ({error})=>{
        this.loadingModalUsuario=false;
        this.errors = error.errors
        // console.log(this.errors);
      }
    )
  }

  openModalEdit(row:any){
    this.errors = [];
    this.editmode= true;
    console.log(row);
    $('#modalUsuario').modal('show');
    this.usuarioForm.setValue({
      id :row.id,
      email :row.email,
      password :'',
      name :row.name
    });
  }

  update(){
    this.errors =[];
    this.loadingModalUsuario = true;
    this.usuarioService.update(this.usuarioForm.value).subscribe(
      (data)=>{
        this.loadingModalUsuario = false;
        console.log(data);
        this.usuarioForm.reset();
        $('#modalUsuario').modal('hide');
        Swal.fire({
          title: 'Satisfactorio',
          text: `${data.message}`,
          icon: 'success',
          confirmButtonText: 'Aceptar',
        }).then((result) => {
            this.all();
        })
      },
      ({error})=>{
        this.loadingModalUsuario = false;
        console.log(error.message);
        this.errors = error.errors
      }
    )
  }

  all(){
    this.usuarioService.all().subscribe(
      (data)=>{
        console.log(data);
        this.usuarios = data;
      },
      (error)=>{console.log(error.message);}
    )
  }


  removeUser(id:any){
    Swal.fire({
      icon:'warning',
      title: '¿Estás seguro?',
      text: 'Estás seguro de que deseas eliminar el usuario',
      showDenyButton: true,
      denyButtonText: `No, cancelar`,
      confirmButtonText: 'Si, continuar',
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.usuarioService.remove(id).subscribe(
          (data)=>{
            $('#modalUsuario').modal('hide');
            Swal.fire({
              title: 'Satisfactorio',
              text: `${data.message}`,
              icon: 'success',
              confirmButtonText: 'Aceptar',
            }).then((result) => {
                this.all();
            })
          }, ({error})=>{
            console.log(error);
          }
        )
      } 
    })
  }

  habilitarUser(id:any){
    this.usuarioService.habilitarUsuario(id).subscribe(
      (data)=>{
        console.log(data);
            $('#modalUsuario').modal('hide');
            Swal.fire({
              title: 'Satisfactorio',
              text: `${data.message}`,
              icon: 'success',
              confirmButtonText: 'Aceptar',
            }).then((result) => {
                this.all();
            })
      },
      ({error})=>{
        console.log(error);
      }
    )
  }

}

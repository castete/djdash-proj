import { UsuarioService } from './../../services/usuario.service';
import { Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loading=false;
  public formSubmited = false;

  public loginForm = this._form.group({
    email:['', [Validators.required]],
    password:['', [Validators.required]],
    remember:[false],
  });

  constructor( private _router:Router,
               private _form:FormBuilder,
               private usuarioService: UsuarioService        
    ) { }

  ngOnInit(): void {
  }

  login(){
    if (this.loginForm.valid) {
      this.loading = true;
      this.usuarioService.login(this.loginForm.value)
      .subscribe(resp => {
        // console.log(resp);
        this.loading = false;
        localStorage.setItem('token',resp.access_token);
        localStorage.setItem('name',resp.user_name);
        localStorage.setItem('role',JSON.stringify(resp.role));
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Bienvenido',
          showConfirmButton: false,
          timer: 1500
        });

        this._router.navigate(['/edits']);
      }, ({error})=>{
        this.loading = false;
        Swal.fire({
          title: 'Lo siento',
          text: `${error.message}`,
          icon: 'warning',
          confirmButtonText: 'Aceptar',
        })
      })
    } else {
      Swal.fire({
        title: 'Lo siento',
        text: 'Datos incompletos',
        icon: 'warning',
        confirmButtonText: 'Aceptar',
      })
    }

  }

}

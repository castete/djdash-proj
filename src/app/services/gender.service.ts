import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GenderService {

  constructor(private http:HttpClient) { }

  
  create(data:any):Observable<any>{
    return this.http.post(`${environment.url}/gender`, data);
  }

  all():Observable<any>{
    return this.http.get(`${environment.url}/gender`);
  }

  update(data:any):Observable<any>{
    return this.http.put(`${environment.url}/gender/${data.id}`, data);
  }

  remove(id:any):Observable<any>{
    return this.http.delete(`${environment.url}/gender/${id}`);
  }
}

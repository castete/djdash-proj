import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SingerService {

  constructor(private http:HttpClient) { }

  
  create(data:any):Observable<any>{
    return this.http.post(`${environment.url}/singer`, data);
  }

  all():Observable<any>{
    return this.http.get(`${environment.url}/singer`);
  }

  update(data:any):Observable<any>{
    return this.http.put(`${environment.url}/singer/${data.id}`, data);
  }

  remove(id:any):Observable<any>{
    return this.http.delete(`${environment.url}/singer/${id}`);
  }
}

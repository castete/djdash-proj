import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  constructor(private http:HttpClient) { }


  create(data:any):Observable<any>{
    return this.http.post(`${environment.url}/edit`, data);
  }

  all():Observable<any>{
    return this.http.get(`${environment.url}/edit`);
  }

  update(data:any):Observable<any>{
    return this.http.post(`${environment.url}/updateedit`, data);
  }

  remove(id:any):Observable<any>{
    return this.http.delete(`${environment.url}/edit/${id}`);
  }


  descarga(data:any):Observable<any>{
    const headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(`${environment.url}/descarga`,data,{ headers,responseType: 'blob' as 'json' });
  }
}

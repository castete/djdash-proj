import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  
  constructor(private http:HttpClient) { }


  login(data:any):Observable<any>{
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Content-Type':  'application/json',
    //   })
    // };
    // this.http.get(`${environment.url}/sanctum/csrf-cokkie`).subscribe(
    //   data=>{
        return this.http.post(`${environment.url}/login`, data);
    //   }, error => {
    //     console.log(error.message);
    //   }
    // )
      // headers: new HttpHeaders({
      //   'Content-Type':  'application/json'
      // })
    
  }

  infoUser():Observable<any>{
    return this.http.get(`${environment.url}/infouser`);
  }

  register(data:any):Observable<any>{
    return this.http.post(`${environment.url}/register`, data);
  }

  all():Observable<any>{
    return this.http.get(`${environment.url}/usuario`);
  }

  update(data:any):Observable<any>{
    return this.http.put(`${environment.url}/usuario/${data.id}`, data);
  }

  remove(id:any):Observable<any>{
    return this.http.delete(`${environment.url}/usuario/${id}`);
  }

  habilitarUsuario(id:any):Observable<any>{
    return this.http.get(`${environment.url}/usuario/habilitar/${id}`);
  }
}
